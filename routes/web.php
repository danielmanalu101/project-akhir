<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\gamecontroller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//branch


Route::get('/index', function () {
    return view('layouts.erd');
});
Route::get('/', function () {
    return view('welcome');
});

Route::get('/tabel', function () {
    return view('layouts.tabel');
});

Route::get('/game', [gamecontroller::class,'index']);
Route::get('/game/create', [gamecontroller::class,'create']);
Route::post('/game/{id}', [gamecontroller::class,'show']);
Route::get('/game/{id}/edit', [gamecontroller::class,'edit']);
Route::put('/game/{id}/', [gamecontroller::class,'update']);
Route::delete('/game/{id}', [gamecontroller::class,'destroy']);

?>
