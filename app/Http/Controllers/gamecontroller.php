<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class gamecontroller extends Controller
{
    public function index()
    {
        $games = DB::table('game')->get();
        return view('game.index', ['game'=> $games]);
    }
    public function create()
    {
        return view('game.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'tahun' => 'required',
        ]);

        DB::table('users')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'tahun' => $request['tahun'],

        ]);
        return view('/game');
    }

    public function show($id)
    {
        $games = DB::table('game')->find($id);
        return view('game.show', ['game'=> $games]);
    }
}

?>


